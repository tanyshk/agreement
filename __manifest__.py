# -*- coding: utf-8 -*-
{
    'name': "Agreement",

    'summary': """""",

    'description': """""",

    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail', 'contacts'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/sequence_data.xml',
        'data/cron_data.xml',
        'data/mail_template_data.xml',
        'views/views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [ ],
    'application': True,
    'sequence': 1,
    'license': 'LGPL-3'
}
