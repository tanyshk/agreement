# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime

import logging
_logger = logging.getLogger(__name__)


class agreementtype(models.Model):
    """Справочник видов договоров"""
    _name = 'agreement.agreementtype'
    _description = 'Справочник видов договоров'
    _inherit = ['mail.thread']
    _sql_constraints = [
        ('name_unique', 'unique(name)', 'Название должно быть уникальным'),
    ]

    name = fields.Char('Название', required = True, tracking = True)
    active = fields.Boolean('Активно', default = True, tracking = True)


class agreement(models.Model):
    """Договоры"""
    _name = 'agreement.agreement'
    _description = 'Договоры'
    _inherit = ['mail.thread']

    def _get_user_def(self):
        return self.env.user.id

    number = fields.Char("Номер", readonly = True, index = True)
    partner_id = fields.Many2one('res.partner', string = 'Клиент', required = True, tracking = True)
    kind_id = fields.Many2one('agreement.agreementtype', string = 'Вид договора', required = True, tracking = True)
    state = fields.Selection(
        [('draft', 'Черновик'), ('approval', 'На согласовании'), ('active', 'Активен'), ('completed', 'Завершен')],
        string = 'Статус', required = True, default = 'draft', tracking = True
    )
    start_date = fields.Date('Дата начала', required = True, tracking = True)
    end_date = fields.Date('Дата завершения', required = True, tracking = True)
    author_id = fields.Many2one('res.users', string='Автор', required = True, readonly = True, default = _get_user_def)
    is_owner = fields.Boolean(compute='_compute_is_owner')

    def _compute_is_owner(self):
        _logger.info("=============== _compute is owner ================")
        self.is_owner = self.author_id.id == self.env.user.id
    @api.model
    def create(self, vals):
        _logger.info("=============== create ================")
        _logger.info(vals['author_id'])
        vals['number'] = self.env['ir.sequence'].next_by_code('agreement.agreement')
        vals['author_id'] = self.env.user.id
        return super(agreement, self).create(vals)

    @api.model
    def _update_state(self):
        _logger.info("=============== _update_state ================")
        current_date = str(datetime.now().date())
        # Ищет договора в статусе “Активен” и датой завершения позже текущей и меняет их статус на “Завершен”
        self.search([('state', '=', 'active'), ('end_date', '>', current_date)]).write({'state': 'completed'})

    def to_approve(self):
        if self.state == 'draft':
            self.state = 'approval'

    def approve(self):
        if self.state == 'approval':
            self.state = 'active'

    def revision(self):
        if self.state == 'approval':
            self.state = 'draft'
            # send email
            template = self.env.ref('agreement.agreement_mail_template_notification')
            template.send_mail(
                self.id,
                force_send=True,
                notif_layout='mail.mail_notification_light',
            )
            _logger.info('Email send')